/**
 * @file
 * JavaScript behaviors for the front-end display of instagram.
 */

(function ($) {

  'use strict';
  Drupal.behaviors.instagram_domain = {
    attach: function (context, settings) {
      var data = settings.instagram_domain;
      $(function () {
        var instagram_username = data.instagram_username;
        var display_profile = data.instagram_display_profile;
        var display_biography = data.instagram_display_biography;
        var items = data.instagram_items;
        var styling = (data.instagram_styling === 'true' ? true : false);
        var items_per_row = data.instagram_items_per_row;
        var settings = {
          username: instagram_username,
          container: '#instagram_post_data',
          display_profile: display_profile,
          display_biography: display_biography,
          display_gallery: true,
          callback: null,
          styling: styling,
          items: items,
          margin: 0.25
        };

        if (styling) {
          settings.items_per_row = items_per_row;
        }

        $.instagramFeed(settings);
      });
    }
  };

})(jQuery);

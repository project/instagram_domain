INSTAGRAM DOMAIN
----------------

INTRODUCTION
------------
Instagram Domain is an integration module for generating public instagram post
with the help of jquery.instagramFeed.min.js library.
By using this module we can place our public instagram post into our
website based on multi-domain configuration.

BENEFITS
--------
As you know many Instagram integrations require access tocken to get instagram
post but, this module does not require a complicated token and authorization
sequence to use.
Simply configure Instagram block by using block GUI and place this block into
particular region where you want to show.

REQUIREMENTS
------------
Drupal 7.x
Domain module : https://www.drupal.org/project/domain

INSTALLATION
------------

Install the Instagram Domain Block:
  Using DRUSH: drush en instagram_domain
  -or-
  Download it from https://www.drupal.org/project/instagram_domain and i
  nstall it to your website.

CONFIGURATION
-------------
Once you have installed Instagram Domain Block then navigate to
Structure -> Block Layout (/admin/structure/block) to create a new Instagram
Block on your site. By default the block will use the Instagram
account and display 12 images, 6 images per row. You can change the Instagram
user account, number of images and number of images per row settings as well as
displaying the Profile and Bio for the Instagram account.
